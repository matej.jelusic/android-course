package hr.fer.ruazosa.prvadomacazadaca;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.HashMap;

import util.Complex;
import util.Operations;

public class MainActivity extends AppCompatActivity {

    private static HashMap<String, Operations> mapaOperacija = new HashMap<String, Operations>();
    static {
        mapaOperacija.put("Zbroji", Operations.ADD);
        mapaOperacija.put("Oduzmi", Operations.SUBSTRACT);
        mapaOperacija.put("Pomnoži", Operations.MULTIPLY);
        mapaOperacija.put("Podijeli", Operations.DEVIDE);
    }

    private Complex prviOperand;
    private Complex drugiOperand;
    private Complex rezultat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button izracunaj = (Button) findViewById(R.id.izracunajGumb);
        izracunaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView rezultatPrikaz = (TextView) findViewById(R.id.rezultat);

                try{
                    osvjeziOperande();
                    rezultatPrikaz.setText(rezultat.toString());
                } catch (Exception e) {
                    rezultatPrikaz.setText("Pogreška");
                }
            }
        });

        Button grafikaGumb = (Button) findViewById(R.id.grafikaGumb);

        grafikaGumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), GrafActivity.class);
                try {
                    osvjeziOperande();
                } catch (Exception e) {
                    TextView rezultatPrikaz = (TextView) findViewById(R.id.rezultat);
                    rezultatPrikaz.setText("Pogreška");
                    return;
                }
                intent.putExtra("prviOperand", prviOperand);
                intent.putExtra("drugiOperand", drugiOperand);
                intent.putExtra("rezultat", rezultat);
                startActivity(intent);
            }
        });


    }
    private void osvjeziOperande(){

        TextView realniDio1TextView = (TextView) findViewById(R.id.realniDioUlaz1);
        TextView imaginarniDio1TextView = (TextView) findViewById(R.id.imaginarniDioUlaz1);
        TextView realniDio2TextView = (TextView) findViewById(R.id.realniDioUlaz2);
        TextView imaginarniDio2TextView = (TextView) findViewById(R.id.imaginarniDioUlaz2);

        prviOperand = new Complex(Double.parseDouble(realniDio1TextView.getText().toString()), Double.parseDouble(imaginarniDio1TextView.getText().toString()));
        drugiOperand = new Complex(Double.parseDouble(realniDio2TextView.getText().toString()), Double.parseDouble(imaginarniDio2TextView.getText().toString()));

        RadioGroup operacija = (RadioGroup) findViewById(R.id.operacija);
        RadioButton operacijaID = (RadioButton) findViewById(operacija.getCheckedRadioButtonId());
        rezultat = mapaOperacija.get(operacijaID.getText()).execute(prviOperand,drugiOperand);
    }
}

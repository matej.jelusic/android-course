package util;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * The Class Complex implements a support for working with complex numbers. This
 * class represents an unmodifiable complex number. Class contains general
 * attributes as real and imaginary part of complex number.
 *
 * @author Matej Jelušić
 * @version 1.0
 */
public class Complex implements Serializable{

	/**
	 * Complex number 0 + 0i.
	 */
	public static final Complex ZERO = new Complex(0, 0);
	/**
	 * Complex number 1 + 0i.
	 * 
	 */
	public static final Complex ONE = new Complex(1, 0);
	/**
	 * Complex number -1 + 0i.
	 * 
	 */
	public static final Complex ONE_NEG = new Complex(-1, 0);
	/**
	 * Complex number 0 + 1i.
	 * 
	 */
	public static final Complex IM = new Complex(0, 1);
	/**
	 * Complex number 0 - 1i.
	 * 
	 */
	public static final Complex IM_NEG = new Complex(0, -1);

	/** The real part of a complex number. */
	private double real;

	/** The imaginary part of a complex number. */
	private double imaginary;

	/**
	 * Instantiates a new complex number.
	 *
	 * @param real
	 *            the real part of complex number
	 * @param imaginary
	 *            the imaginary part of complex number
	 */
	public Complex(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	/**
	 * Instantiates a new complex number.
	 *
	 */
	public Complex() {
		this(0, 0);
	}

	/**
	 * Mehtod that returns module of complex number.
	 * 
	 * @return module of complex number
	 */
	public double module() {
		return Math.sqrt(real * real + imaginary * imaginary);
	}

	/**
	 * A method that multiplys given complex number to this complex number.
	 *
	 * @param c
	 *            second factor
	 * @return product of first and second complex numbers
	 */
	public Complex multiply(Complex c) {
		if (c == null) {
			return this;
		}
		double newReal = real * c.real - imaginary * c.imaginary;
		double newImaginary = real * c.imaginary + imaginary * c.real;
		return new Complex(newReal, newImaginary);
	}

	/**
	 * A method that divides this complex number with this complex number.
	 *
	 * @param c
	 *            divisor(the complex number)
	 * @return quotient(the complex number)
	 */
	public Complex divide(Complex c) {
		if (c == null) {
			return this;
		}
		Complex temporary = this.multiply(new Complex(c.real, -c.imaginary));
		double divisor = c.real * c.real + c.imaginary * c.imaginary;
		if (divisor == 0) {
			throw new IllegalArgumentException("Dividing by zero!");
		}
		return new Complex(temporary.real / divisor, temporary.imaginary / divisor);
	}

	/**
	 * A method that sums given complex number to this complex number.
	 *
	 * @param c
	 *            second addend
	 * @return the sum of the both complex number
	 */
	public Complex add(Complex c) {
		if (c == null) {
			return this;
		}
		return new Complex(real + c.real, imaginary + c.imaginary);
	}

	/**
	 * A method that substracts given complex number from this complex number.
	 *
	 * @param c
	 *            the subtrahend
	 * @return difference of the both complex numbers
	 */
	public Complex sub(Complex c) {
		if (c == null) {
			return this;
		}
		return new Complex(real - c.real, imaginary - c.imaginary);
	}

	/**
	 * Method that returns negative complex complex number.
	 *
	 * @return negated complex number
	 */
	public Complex negate() {
		return new Complex(-real, -imaginary);
	}

	/**
	 * This method calculates n-th power of this complex number using De
	 * Moivre's formula.
	 *
	 * @param n
	 *            the exponent
	 * @return the complex number
	 * @throws IllegalArgumentException
	 *             if n < 0
	 */
	public Complex power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("The exponent should not be negative!");
		}
		double magnitude = Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
		double angle = Math.atan2(imaginary, real);
		return new Complex(Math.pow(magnitude, n) * Math.cos(n * angle), Math.pow(magnitude, n) * Math.sin(n * angle));
	}

	/**
	 * This method calculates n-th root of this complex number using De Moivre's
	 * formula and returns results as array.
	 *
	 * @param n
	 *            the root
	 * @return results of n-th root of this complex number
	 * @throws IllegalArgumentException
	 *             if n <= 0
	 */
	public Complex[] root(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("The exponent should be greater then zero!");
		}
		Complex[] array = new Complex[n];
		double magnitude = Math.sqrt(Math.pow(real, 2) + Math.pow(imaginary, 2));
		double angle = Math.atan2(imaginary, real);
		for (int i = 0; i < n; i++) {
			double x = Math.pow(magnitude, 1 / (double) n) * Math.cos((angle + 2 * i * Math.PI) / n);
			double y = Math.pow(magnitude, 1 / (double) n) * Math.sin((angle + 2 * i * Math.PI) / n);
			array[i] = new Complex(x, y);
		}
		return array;
	}

	@Override
	public String toString() {
		DecimalFormat formatter = new DecimalFormat("#.###");
		return formatter.format(real) + (imaginary > 0 ? " + " : " - ") + formatter.format(Math.abs(imaginary)) + "i";
	}

	/**
	 * Gets the real part of complex.
	 *
	 * @return the real
	 */
	public double getReal() {
		return real;
	}

	/**
	 * Gets the imaginary of complex.
	 *
	 * @return the imaginary
	 */
	public double getImaginary() {
		return imaginary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(imaginary);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(real);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Complex other = (Complex) obj;
		if (Double.doubleToLongBits(imaginary) != Double.doubleToLongBits(other.imaginary)) {
			return false;
		}
		if (Double.doubleToLongBits(real) != Double.doubleToLongBits(other.real)) {
			return false;
		}
		return true;
	}

}

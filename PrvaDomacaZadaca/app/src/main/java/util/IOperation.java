package util;

/**
 * Created by matejjj18 on 28.3.2017..
 */

public interface IOperation {
    public Complex execute(Complex c1, Complex c2);
}

package util;

/**
 * Created by matejjj18 on 28.3.2017..
 */

public enum Operations implements IOperation {
    ADD {
        @Override
        public Complex execute(Complex c1, Complex c2) {
            return c1.add(c2);
        }
    },
    SUBSTRACT {
        @Override
        public Complex execute(Complex c1, Complex c2) {
            return c1.sub(c2);
        }
    },
    MULTIPLY {
        @Override
        public Complex execute(Complex c1, Complex c2) {
            return c1.multiply(c2);
        }
    },
    DEVIDE {
        @Override
        public Complex execute(Complex c1, Complex c2) {
            return c1.divide(c2);
        }
    };
}
